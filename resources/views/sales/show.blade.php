@extends('layouts.admin')

@section('admincontent')

<div class="section__content section__content--p30">
    <div class="container-fluid">
        <h3>Sale {{$sale->reference_number}}</h3>
        <div class="row m-t-30">
            <div class="col-lg-6">
                <!-- TOP CAMPAIGN-->
                <div class="top-campaign">
                    <h3 class="title-3 m-b-30">details</h3>
                    <div class="table-responsive">
                        <table class="table table-top-campaign">
                            <tbody>
                                <tr>
                                    <td>Date</td>
                                    <td>{{$sale->sale_date}}</td>
                                </tr>
                                <tr>
                                    <td>Reference number</td>
                                    <td>{{$sale->reference_number}}</td>
                                </tr>
                                <tr>
                                    <td>Device</td>
                                    <td>
                                        <a href="{{route('devices.show', $sale->device)}}">
                                            <span class="block-email">{{$sale->device->name}}</span>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Products</td>
                                    <td>{{$sale->sale_products->count()}}</td>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td>{{$sale->total}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END TOP CAMPAIGN-->
            </div>
            <div class="col-lg-6">
                <!-- TOP CAMPAIGN-->
                <div class="top-campaign">
                    <h3 class="title-3 m-b-30">additional details</h3>
                    <div class="table-responsive">
                        <table class="table table-top-campaign">
                            <tbody>
                                @foreach ($sale->additional_details as $key => $value)
                                    <tr>
                                        <td>{{$key}}</td>
                                        <td>{{$value}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END TOP CAMPAIGN-->
            </div>
        </div>

        @include('includes.saleproducts', ['saleproducts' => $sale->sale_products])
    </div>
</div>

@endsection