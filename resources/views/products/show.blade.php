@extends('layouts.admin')

@section('admincontent')

<h3 class="title-5 m-b-35">{{$product->name}} sales</h3>
<div class="table-data__tool">
    <div class="table-data__tool-left">
        <div class="rs-select2--light rs-select2--md">
            <select class="js-select2" name="property">
                <option selected="selected">All Properties</option>
                <option value="">CSV</option>
                <option value="">PDF</option>
            </select>
            <div class="dropDownSelect2"></div>
        </div>
        <div class="rs-select2--light rs-select2--sm">
            <select class="js-select2" name="time">
                <option selected="selected">Today</option>
                <option value="">3 Days</option>
                <option value="">1 Week</option>
            </select>
            <div class="dropDownSelect2"></div>
        </div>
        <button class="au-btn-filter">
            <i class="zmdi zmdi-filter-list"></i>filters</button>
    </div>
    <div class="table-data__tool-right">
        <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
            <select class="js-select2" name="type">
                <option selected="selected">Export</option>
                <option value="">Option 1</option>
                <option value="">Option 2</option>
            </select>
            <div class="dropDownSelect2"></div>
        </div>
    </div>
</div>
<div class="table-responsive table-responsive-data2">
    <table class="table table-data2 datatable">
        <thead>
            <tr>
                <th>date</th>
                <th>sale</th>
                <th>device</th>
                <th>price</th>
                <th>quantity</th>
                <th>options</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($product->sale_products as $sale_product)
                <tr class="tr-shadow">
                    <td>{{$sale_product->sale->sale_date}}</td>
                    <td>
                        <a href="{{route('sales.show', $sale_product->sale)}}">
                            <span class="block-email">{{$sale_product->sale->reference_number}}</span>
                        </a>
                    </td>
                    <td>
                        <a href="{{route('devices.show', $sale_product->sale->device)}}">
                            <span class="block-email">{{$sale_product->sale->device->name}}</span>
                        </a>
                    </td>
                    <td>{{$sale_product->price}}</td>
                    <td>{{$sale_product->quantity}}</td>
                    <td>
                        <div class="table-data-feature">
                            <a href="{{route('sales.download', $sale_product->sale)}}" class="item" data-toggle="tooltip" data-placement="top" title="Download Receipt">
                                <i class="zmdi zmdi-download"></i>
                            </a>
                            <a href="{{route('sales.show', $sale_product->sale)}}" class="item" data-toggle="tooltip" data-placement="top" title="More">
                                <i class="zmdi zmdi-more"></i>
                            </a>
                        </div>
                    </td>
                </tr>
                <div class="spacer"></div>
            @endforeach
        </tbody>
    </table>
</div>

@endsection