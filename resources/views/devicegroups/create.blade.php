@extends('layouts.admin')

@section('admincontent')
    
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="flex-wrap">
                    <div class="card">
                        {{-- <div class="card-header">{{ __('Register') }}</div> --}}
                        <div class="card-header">
                            <a href="/">
                                <img src="/images/icon/logo-mini.png" alt="company name">
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="card-title">
                                <h3 class="text-center title-2">Add Device Group</h3>
                            </div>
                            <form method="POST" action="{{ route('devicegroups.store') }}">
                                @csrf

                                @include('includes.forminputfields',['fieldname' => 'name', 'fieldtype' => 'text', 'attributes' => 'required autofocus'])

                                @include('includes.forminputfields',['fieldtype' => 'submit', 'fieldlabel' => 'Add device group'])
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>

@endsection
