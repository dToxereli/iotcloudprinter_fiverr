<div class="table-responsive">
    <table class="table table-top-campaign">
        <tbody>
            @foreach ($top_products as $product)
                <tr>
                    <td>{{$product->name}}</td>
                    <td>{{$product->sold_quantity}} {{str_plural('sale', $product->sold_quantity)}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>