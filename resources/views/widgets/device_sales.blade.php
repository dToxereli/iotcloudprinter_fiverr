<div class="table-responsive m-b-40">
    <table class="table table-borderless table-data3 datatable">
        <thead>
            <tr>
                <th>device</th>
                <th>sales</th>
                <th>earnings</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($devices as $device)
                <tr>
                    <td>
                        <a href="{{route('devices.show', $device)}}">
                            <span class="block-email">{{$device->name}}</span>
                        </a>
                    </td>
                    <td>{{$device->sales->count()}}</td>
                    <td>{{$device->sales->sum('total')}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>