<div class="table-responsive">
    <table class="table table-top-campaign">
        <tbody>
            @foreach ($top_products as $product)
                <tr>
                    <td>{{$product->name}}</td>
                    <td>{{$product->sales_count}} {{str_plural('sale', $product->sales_count)}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>