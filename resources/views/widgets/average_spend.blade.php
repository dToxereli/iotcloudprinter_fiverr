@component('components.statisticcard')
    @slot('icon')
        <i class="zmdi zmdi-calendar-note"></i>
    @endslot
    <h2>{{$average_spend}}</h2>
    <span>Average consumer spend</span>
@endcomponent