@component('components.statisticcard')
    @slot('icon')
        <i class="zmdi zmdi-money"></i>
    @endslot
    <h2>{{$total_earnings}}</h2>
    <span>total earnings</span>
@endcomponent