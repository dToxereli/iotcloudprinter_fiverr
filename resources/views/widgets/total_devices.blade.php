@component('components.statisticcard')
    @slot('icon')
        <i class="zmdi zmdi-devices"></i>
    @endslot
    <h2>{{$device_count}}</h2>
    <span>Total devices</span>
@endcomponent