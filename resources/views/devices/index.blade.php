@extends('layouts.admin')

@section('admincontent')

<div class="row">
    <div class="col-md-12">
        @include('includes.devices')
    </div>
</div>

@endsection
