@extends('layouts.admin')

@section('admincontent')

<div class="section__content section__content--p30">
    <div class="container-fluid">
        @component('components.notification', ['class' => 'info'])
            API Token: {{$device->api_token}}
        @endcomponent

        <div class="row m-t-30">
            <div class="col-lg-12">
                <!-- TOP CAMPAIGN-->
                <div class="top-campaign">
                    <h3 class="title-3 m-b-30">details</h3>
                    <div class="table-responsive">
                        <table class="table table-top-campaign">
                            <tbody>
                                <tr>
                                    <td>Name</td>
                                    <td>{{$device->name}}</td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>
                                        <span class="status--{{$device->is_active ? 'process' : 'denied'}}">{{$device->is_active ? 'Active' : 'Disabled'}}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Sales</td>
                                    <td>{{$device->sales->count()}}</td>
                                </tr>
                                <tr>
                                    <td>Gross</td>
                                    <td>{{$device->sales->sum('total')}}</td>
                                </tr>
                                <tr>
                                    <td>Date added</td>
                                    <td>{{$device->created_at}}</td>
                                </tr>
                                <tr>
                                    <td>Groups</td>
                                    <td>
                                        <ul class="list-unstyled">
                                            @foreach ($device->device_groups as $device_group)
                                                <li>
                                                    <a href="{{route('devicegroups.show', $device_group)}}">
                                                        <span class="block-email">{{$device_group->name}}</span>
                                                    </a>
                                                </li>
                                                <br>
                                            @endforeach
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <a class="au-btn au-btn-icon au-btn--blue2 au-btn--small" href="{{route('devices.edit', $device)}}">
                        <i class="zmdi zmdi-edit"></i>edit device</a>
                </div>
                <!-- END TOP CAMPAIGN-->
            </div>
        </div>
    </div>
</div>

@include('includes.sales', ['sales' => $device->sales])

@endsection
