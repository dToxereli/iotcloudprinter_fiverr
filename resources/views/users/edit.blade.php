@extends('layouts.admin')

@section('admincontent')
    
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="flex-wrap">
                    <div class="card">
                        {{-- <div class="card-header">{{ __('Register') }}</div> --}}
                        <div class="card-header">
                            <a href="/">
                                <img src="/images/icon/logo-mini.png" alt="company name">
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="card-title">
                                <h3 class="text-center title-2">edit {{$user->name}}</h3>
                            </div>
                            <form method="POST" action="{{ route('users.update', $user) }}" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')

                                @include('includes.forminputfields',['fieldname' => 'device_limit', 'fieldlabel' => 'Device limit', 'fieldtype' => 'number','fielddefault' => $user->device_limit > 0 ? $user->device_limit : '', 'attributes' => 'required autofocus min="1" step-size="1"'])

                                @include('includes.forminputfields',[
                                    'fieldname' => 'is_active',
                                    'fieldtype' => 'select',
                                    'fieldlabel' => 'Status',
                                    'fielddefault' => $user->status,
                                    'fieldoptions' => [
                                        '0' => 'Disabled',
                                        '1' => 'Active'
                                    ],
                                    'attributes' => 'required'
                                ])

                                {{-- <div class="form-group row">
                                    <p class="text-center">
                                        @if ($errors->has('group_ids.*'))
                                            <span class="text-danger error">
                                                <strong>{{ $errors->first('group_ids.*') }}</strong>
                                            </span>
                                        @endif
                                    </p>
                                    <div class="table-responsive m-b-40">
                                        <table class="table table-borderless table-data2 datatable">
                                            <thead>
                                                <tr>
                                                    <th>Assign to User Groups</th>
                                                    <th>Created by</th>
                                                    <th>user count</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @for ($i = 0; $i < 5; $i++)
                                                    <tr>
                                                        <td>
                                                            <div class="form-group row">
                                                                <div class="col-md-1">
                                                                    <input id="group{{$i}}" type="checkbox" class="form-check-input" name="group{{$i}}" value="{{$i}}">
                                                                </div>
                                                                <label for="group{{$i}}" class="form-check-label col-md-4"> Group {{$i + 1}} </label>
                                                            </div>
                                                        </td>
                                                        <td>User 1</td>
                                                        <td>3</td>
                                                    </tr>
                                                @endfor
                                            </tbody>
                                        </table>
                                    </div>
                                </div> --}}

                                @include('includes.forminputfields',['fieldtype' => 'submit', 'fieldlabel' => 'Edit user'])
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>

@endsection
