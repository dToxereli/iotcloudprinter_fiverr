<div class="notifi__item">
    <div class="bg-c1 img-cir img-40">
        {{ $icon }}
    </div>
    <div class="content">
        {{ $slot }}
    </div>
</div>