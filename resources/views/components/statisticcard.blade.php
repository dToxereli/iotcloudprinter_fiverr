<div class="statistic__item">
    {{ $slot }}
    <div class="icon">
        {{ $icon }}
    </div>
</div>