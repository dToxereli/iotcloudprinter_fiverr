<div class="au-task__item au-task__item--{{isset($type) ? $type : 'primary'}}">
    <div class="au-task__item-inner">
        <span class="time">{{$datetime}}</span>
        <h5 class="task">
            @if (isset($link))
                <h5 class="font-weight-light"><a href="{{$link}}">{{$title}}</a></h5>
            @else
                <h5 class="font-weight-light">{{$title}}</h5>
            @endif
        </h5>
        <p class="m-b-20 m-t-20">
            {{$slot}}
        </p>
    </div>
</div>