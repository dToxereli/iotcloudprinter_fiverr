<div class="alert mn-alert alert-{{$class}} alert-dismissible fade show" role="alert">
    <i class="zmdi zmdi-notifications"></i>
    <span class="content overflow-break">{{$slot}}</span>
    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">
            <i class="zmdi zmdi-close-circle"></i>
        </span>
    </button>
</div>