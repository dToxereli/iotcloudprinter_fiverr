@extends('layouts.admin')

@section('admincontent')

<h3 class="title-5">Profile</h3>
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-user"></i>
                            <strong class="card-title pl-2">Profile</strong>
                        </div>
                        <div class="card-body">
                            <div class="mx-auto d-block">
                                <img class="rounded-circle mx-auto d-block" src="{{Auth::user()->imageUrl('picture')}}" alt="Card image cap">
                                <h3 class="text-sm-center mt-2 mb-1 title-1">{{Auth::user()->name}}</h3>
                                <div class="location text-sm-center"><i class="fa fa-envelope"></i> {{Auth::user()->email}}</div>
                            </div>
                        </div>
                        <div class="card-footer text-center">
                            <a href="{{route('profile.edit')}}" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                    <i class="zmdi zmdi-account-add"></i>Edit profile</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
                        <div class="au-card-title" style="background-image:url('images/bg-title-01.jpg');">
                            <div class="bg-overlay bg-overlay--blue"></div>
                            <h3>
                                <i class="zmdi zmdi-account-calendar"></i>26 April, 2018</h3>
                            <button class="au-btn-plus">
                                99+
                            </button>
                        </div>
                        <div class="au-task js-list-load">
                            <div class="au-task__title">
                                <p>Notifications for {{Auth::user()->name}}</p>
                            </div>
                            <div class="au-task-list js-scrollbar3">
                                @component('components.profile_notification', [
                                    'title' => 'Meeting about plan for Admin Template 2018', 
                                    'datetime' => '10:00 AM'])
                                @endcomponent
                                @component('components.profile_notification', [
                                    'title' => 'Meeting about plan for Admin Template 2018', 
                                    'datetime' => '11:00 AM'])
                                @endcomponent
                                @component('components.profile_notification', [
                                    'title' => 'Meeting about plan for Admin Template 2018', 
                                    'datetime' => '02:00 PM'])
                                @endcomponent
                                @component('components.profile_notification', [
                                    'title' => 'Create new task for Dashboard', 
                                    'datetime' => '03:30 PM'])
                                @endcomponent
                                <div class="au-task__item au-task__item--danger js-load-item">
                                    <div class="au-task__item-inner">
                                        <h5 class="task">
                                            <a href="#">Meeting about plan for Admin Template 2018</a>
                                        </h5>
                                        <span class="time">10:00 AM</span>
                                    </div>
                                </div>
                                <div class="au-task__item au-task__item--warning js-load-item">
                                    <div class="au-task__item-inner">
                                        <h5 class="task">
                                            <a href="#">Create new task for Dashboard</a>
                                        </h5>
                                        <span class="time">11:00 AM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="au-task__footer">
                                <button class="au-btn au-btn-load js-load-btn">load more</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
