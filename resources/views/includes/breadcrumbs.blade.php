<div class="au-breadcrumb3">
    <div class="au-breadcrumb-left">
        <span class="au-breadcrumb-span">You are here:</span>
        <ul class="list-unstyled list-inline au-breadcrumb__list">
            <li class="list-inline-item active">
                <a href="/">Home</a>
            </li>
            @foreach (explode("/",Request::path()) as $path)
                @if ($path != "")
                    <li class="list-inline-item seprate">
                        <span>/</span>
                    </li>
                    <li class="list-inline-item">{{ucfirst($path)}}</li>
                @endif
            @endforeach
        </ul>
    </div>
</div>