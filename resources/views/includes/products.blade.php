<div class="row">
    <div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-5 m-b-35">products</h3>
        <div class="table-data__tool">
            <div class="table-data__tool-left">
                <a class="au-btn au-btn-icon au-btn--blue2 au-btn--small" href="{{route('products.export')}}" target="_blank">
                    <i class="fa fa-file-excel"></i> Export CSV
                </a>
            </div>
            <div class="table-data__tool-right"></div>
        </div>
        <div class="table-responsive table-responsive-data2">
            <table class="table table-data2 datatable">
                <thead>
                    <tr>
                        <th>name</th>
                        <th>sold quantity</th>
                        <th>gross</th>
                        <th>actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($products as $product)
                        <tr class="tr-shadow">
                            <td>{{$product->name}}</td>
                            <td>{{$product->sold_quantity}}</td>
                            <td>{{$product->gross}}</td>
                            <td>
                                <div class="table-data-feature">
                                    <a href="{{route('products.show', $product)}}" class="item" data-toggle="tooltip" data-placement="top" title="More">
                                        <i class="zmdi zmdi-more"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <div class="spacer"></div>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
</div>
