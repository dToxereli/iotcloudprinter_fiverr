<div class="row">
    <div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-5 m-b-35">sales</h3>
        <div class="table-data__tool">
            <div class="table-data__tool-left">
                <a class="au-btn au-btn-icon au-btn--blue2 au-btn--small" href="{{route('sales.export')}}" target="_blank">
                    <i class="fa fa-file-excel"></i> Export CSV
                </a>
            </div>
            <div class="table-data__tool-right"></div>
        </div>
        <div class="table-responsive table-responsive-data2">
            <table class="table table-data2 datatable">
                <thead>
                    <tr>
                        <th>date</th>
                        <th>reference_number</th>
                        <th>device</th>
                        <th>products</th>
                        <th>total</th>
                        <th>options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($sales as $sale)
                        <tr class="tr-shadow">
                            <td>{{$sale->sale_date}}</td>
                            <td>{{$sale->reference_number}}</td>
                            <td>
                                <a href="{{route('devices.show', $sale->device)}}">
                                    <span class="block-email">{{$sale->device->name}}</span>
                                </a>
                            </td>
                            <td>{{$sale->sale_products->count()}}</td>
                            <td>{{$sale->total}}</td>
                            <td>
                                <div class="table-data-feature">
                                    <a href="{{route('sales.download', $sale)}}" class="item" data-toggle="tooltip" data-placement="top" title="Download Receipt">
                                        <i class="zmdi zmdi-download"></i>
                                    </a>
                                    <a href="{{route('sales.show', $sale)}}" class="item" data-toggle="tooltip" data-placement="top" title="More">
                                        <i class="zmdi zmdi-more"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <div class="spacer"></div>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
</div>
