<div class="account-wrap">
    <div class="account-item account-item--style2 clearfix js-item-menu">
        <div class="image">
            <img src="{{Auth::user()->imageUrl('picture')}}" alt="John Doe" />
        </div>
        <div class="content">
            <a class="js-acc-btn" href="#">{{Auth::user()->name}}</a>
        </div>
        <div class="account-dropdown js-dropdown">
            <div class="info clearfix">
                <div class="image">
                    <a href="#">
                        <img src="{{Auth::user()->imageUrl('picture')}}" alt="{{Auth::user()->name}}" />
                    </a>
                </div>
                <div class="content">
                    <h5 class="name">
                        <a href="#">{{Auth::user()->name}}</a>
                    </h5>
                    <span class="email">{{Auth::user()->email}}</span>
                </div>
            </div>
            <div class="account-dropdown__body">
                <div class="account-dropdown__item">
                    <a href="{{route('profile.index')}}"><i class="zmdi zmdi-account"></i>Profile</a>
                </div>
            </div>
            <div class="account-dropdown__footer">
                @include('includes.logout')
            </div>
        </div>
    </div>
</div>