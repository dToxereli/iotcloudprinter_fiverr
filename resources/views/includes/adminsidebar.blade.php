<ul class="list-unstyled navbar__list">
    <li class="has-sub">
        <a class="js-arrow" href="#">
            <i class="fas fa-tachometer-alt"></i>Dashboard
            <span class="arrow">
                <i class="fas fa-angle-down"></i>
            </span>
        </a>
        <ul class="list-unstyled navbar__sub-list js-sub-list">
            <li>
                <a href="{{route('dashboard.index')}}">Home</a>
            </li>
            <li>
                <a href="{{route('sales.index')}}">Sales</a>
            </li>
            <li>
                <a href="{{route('products.index')}}">Products</a>
            </li>
        </ul>
    </li>
    <li class="has-sub">
        <a class="js-arrow" href="#">
            <i class="fas fa-microchip"></i>Devices
            <span class="arrow">
                <i class="fas fa-angle-down"></i>
            </span>
        </a>
        <ul class="list-unstyled navbar__sub-list js-sub-list">
            <li>
                <a href="{{route('devices.index')}}">Device management</a>
            </li>
            <li>
                <a href="{{route('devicegroups.index')}}">Device groups</a>
            </li>
        </ul>
    </li>
    <li class="has-sub">
        <a class="js-arrow" href="#">
            <i class="fas fa-users"></i>Users
            <span class="arrow">
                <i class="fas fa-angle-down"></i>
            </span>
        </a>
        <ul class="list-unstyled navbar__sub-list js-sub-list">
            <li>
                <a href="{{route('users.index')}}">Accounts</a>
            </li>
            <li>
                <a href="{{route('usergroups.index')}}">Groups</a>
            </li>
        </ul>
    </li>
    <li class="has-sub">
        <a class="js-arrow" href="#">
            <i class="fas fa-user"></i>Account
            <span class="arrow">
                <i class="fas fa-angle-down"></i>
            </span>
        </a>
        <ul class="list-unstyled navbar__sub-list js-sub-list">
            <li>
                <a href="{{route('profile.index')}}">Profile</a>
            </li>
        </ul>
    </li>
</ul>