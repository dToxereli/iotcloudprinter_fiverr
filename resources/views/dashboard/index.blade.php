@extends('layouts.admin')

@section('admincontent')
    <!-- STATISTIC-->
    <section>
        <div class="row">
            <div class="col-md-12">
                <h3 class="title-5">statistics</h3>
            </div>
        </div>
    </section>
    
    <section class="statistic">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    @widget('totalDevices')
                </div>
                <div class="col-md-6 col-lg-3">
                    @widget('totalSales')
                </div>
                <div class="col-md-6 col-lg-3">
                    @widget('averageSpend')
                </div>
                <div class="col-md-6 col-lg-3">
                    @widget('totalEarnings')
                </div>
            </div>
        </div>
    </section>
    <!-- END STATISTIC-->

    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="top-campaign">
                        <div class="table-data__tool">
                            <div class="table-data__tool-left">
                                <div class="rs-select2--light rs-select2--md">
                                    <h3 class="title-3 m-b-30">top selling</h3>
                                </div>
                            </div>
                            <div class="table-data__tool-right">
                                <form class="ajaxform" action="{{route('dashboard.top_selling_products')}}" method="post" id="top_products_filter" data-target="#top_selling_products">
                                    @csrf
                                    <div class="rs-select2--dark rs-select2--md m-r-10">
                                        <select class="js-select2" name="span" id="top_products_span">
                                            <option selected="selected" value="all_time">All time</option>
                                            <option value="today">Today</option>
                                            <option value="this_week">This week</option>
                                            <option value="this_month">This month</option>
                                            <option value="this_year">This year</option>
                                        </select>
                                        <div class="dropDownSelect2"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div id="top_selling_products">
                            @widget('topSellingProducts')
                        </div>
                    </div>
                    <!--  END TOP CAMPAIGN-->
                </div>
                <div class="col-lg-6">
                    <div class="top-campaign">
                        <div class="table-data__tool">
                            <div class="table-data__tool-left">
                                <div class="rs-select2--light rs-select2--md">
                                    <h3 class="title-3 m-b-30">top grossing</h3>
                                </div>
                            </div>
                            <div class="table-data__tool-right">
                                <form class="ajaxform" action="{{route('dashboard.top_grossing_products')}}" method="post" id="top_products_filter" data-target="#top_grossing_products">
                                    @csrf
                                    <div class="rs-select2--dark rs-select2--md m-r-10">
                                        <select class="js-select2" name="span" id="top_products_span">
                                            <option selected="selected" value="all_time">All time</option>
                                            <option value="today">Today</option>
                                            <option value="this_week">This week</option>
                                            <option value="this_month">This month</option>
                                            <option value="this_year">This year</option>
                                        </select>
                                        <div class="dropDownSelect2"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div id="top_grossing_products">
                            @widget('topGrossingProducts')
                        </div>
                    </div>
                    <!--  END TOP CAMPAIGN-->
                </div>
            </div>
        </div>
    </section>

    <section class="statistic-chart">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="au-card recent-report">
                            <div class="au-card-inner">
                                <h3 class="title-2">recent reports</h3>
                                <div class="chart-info">
                                    <div class="chart-info__left">
                                        <div class="chart-note">
                                            <span class="dot dot--blue"></span>
                                            <span>products</span>
                                        </div>
                                        <div class="chart-note mr-0">
                                            <span class="dot dot--green"></span>
                                            <span>services</span>
                                        </div>
                                    </div>
                                    <div class="chart-info-right">
                                        <form action="{{route('dashboard.sales_gross_chart')}}" method="post" class="ajaxform" data-target="#sales_gross_chart">
                                            @csrf
                                            <div class="rs-select2--dark rs-select2--md m-r-10">
                                                <select class="js-select2" name="summary">
                                                    <option value="sales" selected="selected">Sales</option>
                                                    <option value="gross">Gross</option>
                                                </select>
                                                <div class="dropDownSelect2"></div>
                                            </div>
                                            <div class="rs-select2--dark rs-select2--md m-r-10">
                                                <select class="js-select2 au-select-dark" name="span">
                                                    <option value="all_time" selected="selected">All Time</option>
                                                    <option value="today">Today</option>
                                                    <option value="this_week">This week</option>
                                                    <option value="this_month">This month</option>
                                                    <option value="this_year">This year</option>
                                                </select>
                                                <div class="dropDownSelect2"></div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div id="sales_gross_chart" class="recent-report__chart">
                                    @widget('salesGrossChart')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- STATISTIC CHART-->
    <section class="statistic-chart">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <!-- CHART-->
                    <div class="statistic-chart-1">
                        <h3 class="title-3 m-b-30">payment method gross</h3>
                        <div id="payment_method_gross">
                            @widget('paymentMethodGross')
                        </div>
                    </div>
                    <!-- END CHART-->
                </div>
                <div class="col-lg-6">
                    <!-- CHART-->
                    <div class="statistic-chart-1">
                        <h3 class="title-3 m-b-30">payment method sales</h3>
                        <div id="payment_method_sales">
                            @widget('paymentMethodSales')
                        </div>
                    </div>
                    <!-- END CHART-->
                </div>
            </div>
        </div>
    </section>
    <!-- END STATISTIC CHART-->

    <!-- DATA TABLE-->
    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title-5 m-b-35">device sales</h3>
                    <div class="table-data__tool">
                        <div class="table-data__tool-left">
                            <a class="au-btn au-btn-icon au-btn--blue2 au-btn--small" href="{{route('dashboard.export_devices_csv')}}" target="_blank">
                                <i class="fa fa-file-excel"></i> Export CSV
                            </a>
                        </div>
                        <div class="table-data__tool-right"></div>
                    </div>
                    <div id="device_sales">
                        @widget('deviceSales')
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->
@endsection