@extends('layouts.admin')

@section('admincontent')

<div class="row">
    <div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-5 m-b-35">User groups</h3>
        <div class="table-data__tool">
            <div class="table-data__tool-left">
                <div class="rs-select2--light rs-select2--md">
                    <select class="js-select2" name="property">
                        <option selected="selected">All Properties</option>
                        <option value="">Option 1</option>
                        <option value="">Option 2</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <div class="rs-select2--light rs-select2--sm">
                    <select class="js-select2" name="time">
                        <option selected="selected">Today</option>
                        <option value="">3 Days</option>
                        <option value="">1 Week</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <button class="au-btn-filter">
                    <i class="zmdi zmdi-filter-list"></i>filters</button>
            </div>
            <div class="table-data__tool-right">
                <a href="{{route('usergroups.create')}}" class="au-btn au-btn-icon au-btn--green au-btn--small">
                    <i class="zmdi zmdi-plus"></i>add user group</a>
                <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                    <select class="js-select2" name="type">
                        <option selected="selected">Export</option>
                        <option value="">Option 1</option>
                        <option value="">Option 2</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
            </div>
        </div>
        <div class="table-responsive table-responsive-data">
            <table class="table table-data2 datatable">
                <thead>
                    <tr>
                        <th>name</th>
                        <th>created by</th>
                        <th>users</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="tr-shadow">
                        <td>Device Name</td>
                        <td>
                            <span class="block-email">User 1</span>
                        </td>
                        <td>
                            <span class="status--process">10</span>
                        </td>
                        <td>
                            <div class="table-data-feature">
                                <a href="{{route('usergroups.edit',1)}}" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </a>
                                <form action="{{route('usergroups.destroy',1)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="zmdi zmdi-delete"></i>
                                    </button>
                                </form>
                                <a href="{{route('usergroups.show',1)}}" class="item" data-toggle="tooltip" data-placement="top" title="More">
                                    <i class="zmdi zmdi-more"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                    <div class="spacer"></div>
                    <tr class="tr-shadow">
                        <td>Device Name</td>
                        <td>
                            <span class="block-email">User 2</span>
                        </td>
                        <td>
                            <span class="status--process">15</span>
                        </td>
                        <td>
                            <div class="table-data-feature">
                                <a href="{{route('usergroups.edit',1)}}" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </a>
                                <form action="{{route('usergroups.destroy',1)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="zmdi zmdi-delete"></i>
                                    </button>
                                </form>
                                <a href="{{route('usergroups.show',1)}}" class="item" data-toggle="tooltip" data-placement="top" title="More">
                                    <i class="zmdi zmdi-more"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                    <div class="spacer"></div>
                    <tr class="tr-shadow">
                        <td>Device Name</td>
                        <td>
                            <span class="block-email">User 3</span>
                        </td>
                        <td>
                            <span class="status--denied">0</span>
                        </td>
                        <td>
                            <div class="table-data-feature">
                                <a href="{{route('usergroups.edit',1)}}" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </a>
                                <form action="{{route('usergroups.destroy',1)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="zmdi zmdi-delete"></i>
                                    </button>
                                </form>
                                <a href="{{route('usergroups.show',1)}}" class="item" data-toggle="tooltip" data-placement="top" title="More">
                                    <i class="zmdi zmdi-more"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                    <div class="spacer"></div>
                    <tr class="tr-shadow">
                        <td>Device Name</td>
                        <td>
                            <span class="block-email">User 4</span>
                        </td>
                        <td>
                            <span class="status--process">3</span>
                        </td>
                        <td>
                            <div class="table-data-feature">
                                <a href="{{route('usergroups.edit',1)}}" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </a>
                                <form action="{{route('usergroups.destroy',1)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="zmdi zmdi-delete"></i>
                                    </button>
                                </form>
                                <a href="{{route('usergroups.show',1)}}" class="item" data-toggle="tooltip" data-placement="top" title="More">
                                    <i class="zmdi zmdi-more"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
</div>

@endsection
