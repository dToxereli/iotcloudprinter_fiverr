$.fn.ajaxform = function () {
    var filterform = $(this);
    $(this).find(':input').change(function () {
        filterform.submit();
    });

    filterform.submit(function (event) {
        event.preventDefault();

        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize(),
            success: function( data ) {
                var target = $(filterform.data("target"));
                if (target) {
                    target.html(data);
                }
            },
            error: function( xhr, err ) {
                console.log(err);
            }
        })
    })
}