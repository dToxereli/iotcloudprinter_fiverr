<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('devices', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });
        Schema::table('sale_products', function (Blueprint $table) {
            $table->foreign('sale_id')->references('id')->on('sales')->onDelete('set null');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('set null');
        });
        Schema::table('sale_payments', function (Blueprint $table) {
            $table->foreign('sale_id')->references('id')->on('sales')->onDelete('set null');
        });
        Schema::table('sales', function (Blueprint $table) {
            $table->foreign('device_id')->references('id')->on('devices')->onDelete('set null');
        });
        Schema::table('devices_device_groups', function (Blueprint $table) {
            $table->foreign('device_id')->references('id')->on('devices')->onDelete('cascade');
            $table->foreign('device_group_id')->references('id')->on('device_groups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('devices', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        Schema::table('devices_device_groups', function (Blueprint $table) {
            $table->dropForeign(['device_id']);
            $table->dropForeign(['device_group_id']);
        });

        Schema::table('sales', function (Blueprint $table) {
            $table->dropForeign(['device_id']);
        });

        Schema::table('sale_payments', function (Blueprint $table) {
            $table->dropForeign(['sale_id']);
        });

        Schema::table('sale_products', function (Blueprint $table) {
            $table->dropForeign(['sale_id']);
            $table->dropForeign(['product_id']);
        });
    }
}
