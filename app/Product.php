<?php

namespace App;

use App\Sale;
use Carbon\Carbon;
use App\SaleProduct;
use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasUuid;

    public $incrementing = false;
    
    protected $fillable = ['name'];

    protected $appends = ['gross', 'sold_quantity'];


    public function sale_products()
    {
        return $this->hasMany(SaleProduct::class, 'product_id', 'id');
    }

    public function sales()
    {
        return $this->belongsToMany(Sale::class, 'sale_products', 'product_id', 'sale_id');
    }

    public function getSoldQuantityAttribute()
    {
        return $this->sale_products->sum('quantity');
    }

    public function getGrossAttribute()
    {
        return $this->sale_products->sum('price');
    }

    public function scopeSoldToday($query)
    {
        return $query->whereHas('sales', function($q)
        {
            $q->whereDate('sale_date', '=', Carbon::today()->toDateString());
        });
    }

    public function scopeSoldThisWeek($query)
    {
        return $query->whereHas('sales', function($q)
        {
            $q->whereBetween('sale_date', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
        });
    }

    public function scopeSoldThisMonth($query)
    {
        return $query->whereHas('sales', function($q)
        {
            $q->whereMonth('sale_date', '=', Carbon::today()->month);
        });
    }

    public function scopeSoldThisYear($query)
    {
        return $query->whereHas('sales', function($q)
        {
            $q->whereYear('sale_date', '=', Carbon::today()->year);
        });
    }
}
