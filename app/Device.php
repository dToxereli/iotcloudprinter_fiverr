<?php

namespace App;

use App\Sale;
use App\User;
use App\DeviceGroup;
use App\Traits\HasUuid;
use App\Traits\HasApiKey;
use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    use HasUuid;
    use HasApiKey;

    public $incrementing = false;
    
    public $api_key_field = 'api_token';

    protected $fillable = ['name', 'is_active'];

    protected $hidden = ['api_token'];

    public function device_groups()
    {
        return $this->belongsToMany(DeviceGroup::class, 'devices_device_groups', 'device_id', 'device_group_id');
    }

    public function sales()
    {
        return $this->hasMany(Sale::class, 'device_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
