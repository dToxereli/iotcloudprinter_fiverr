<?php

namespace App\Utilitites;
use Carbon\Carbon;
use Spatie\PdfToText\Pdf;


class PDFParser
{
    public function parse($filepath, $formatted = true)
    {
        $text = Pdf::getText($filepath, null, ['-nopgbrk','-layout']);
        $lines = explode("\n",$text);
        $parsedData = [];
        $products = [];
        $parsedData['details'] = [];
        foreach ($lines as $line) {
            if (preg_match('/- [0-9]+ [a-zA-Z0-9, ]+( )+[0-9]+(\.[0-9]{1,2})/',$line))
            {
                $data = [];
                $line = ltrim($line,"- ");
                $split1 = explode(' ', $line, 2);
                $data['quantity'] = $split1[0];
                $split2 = preg_split('/ [ ]+/',$split1[1]);
                $data['name'] = $split2[0];
                $data['price'] = $split2[1];
                array_push($products, $data);
            } else {
                $split = preg_split('/:[ ]*/',$line,2);
                if (count($split) == 2) {
                    $parsedData['details'][strtolower($split[0])] = $split[1];
                }
            }
        }
        $parsedData['products'] = $products;
        if ($formatted) {
            return $this->format($parsedData);
        }
        return $parsedData;
    }

    public function format($data)
    {
        $formattedData = [];
        $formattedData['details'] = [];
        $formattedData['products'] = [];

        $datetime = $data['details']['date']." ".$data['details']['time'];
        $formattedData['details']['reference_number'] = preg_replace("/(\/| |:)/","",$datetime).''.$data['details']['ticket no'];
        $formattedData['details']['total'] = (float) $data['details']['total'];
        $formattedData['details']['sale_date'] = new Carbon(date("m/d/y H:i", strtotime($datetime)));
        $formattedData['details']['method'] = isset ($data['details']['method']) ? $data['details']['method'] : 'Cash';
        $formattedData['details']['additional_details'] = $data['details'];

        foreach ($data['products'] as $product) {
            $product['quantity'] = (int)$product['quantity'];
            $product['price'] = (float)$product['price'];
            array_push($formattedData['products'], $product);
        }

        return $formattedData;
    }
}
