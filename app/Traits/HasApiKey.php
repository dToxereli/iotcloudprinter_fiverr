<?php

namespace App\Traits;

use Illuminate\Support\Str;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

trait HasApiKey
{
    /**
     * Boot function from laravel.
     */
    protected static function bootHasApiKey()
    {
        static::creating(function ($model) {
            $model->{$model->api_key_field} = Str::random(60);
        });
    }
}