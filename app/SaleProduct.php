<?php

namespace App;

use App\Sale;
use App\Product;
use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;

class SaleProduct extends Model
{
    use HasUuid;

    public $incrementing = false;
    
    protected $fillable = ['price', 'quantity', 'product_id', 'sale_id'];
    
    public function sale()
    {
        return $this->belongsTo(Sale::class, 'sale_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
