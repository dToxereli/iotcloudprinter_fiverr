<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Chartjs\Chart;

class DeviceSaleComparisonChart extends Chart
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->options([
            'legend'=> [
                'display' => false
            ],
            'responsive' => true,
            'scales' => [
                'xAxes' => [[
                'gridLines' => [
                    'drawOnChartArea' => true,
                    'color' => '#f2f2f2'
                ],
                'ticks' => [
                    'fontFamily' => "Poppins",
                    'fontSize' => 12
                ]
                ]],
                'yAxes' => [[
                'ticks' => [
                    'beginAtZero' => true,
                    'maxTicksLimit' => 5,
                    'fontFamily' => "Poppins",
                    'fontSize' => 12
                ],
                'gridLines' => [
                    'display' => true,
                    'color' => '#f2f2f2'
    
                ]
                ]]
            ],
        ]);
    }
}
