<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Response;

class ProductController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('sales')->withCount('sale_products')->get();
        return view('products.index', compact("products"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $product->load(['sale_products', 'sale_products.sale', 'sale_products.sale.device']);
        return view('products.show', compact("product"));
    }

    public function export()
    {
        $headers = [
                'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=products.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        $products = Product::with('sale_products')->get();

        $data = $products->map(function ($product)
        {
            return [
                $product->name, 
                $product->sold_quantity, 
                $product->gross,
            ];
        })->toArray();

        array_unshift($data, ['Name', 'Sold quantity', 'Gross']);

        $callback = function() use ($data) 
        {
            $FH = fopen('php://output', 'w');
            foreach ($data as $row) { 
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return Response::stream($callback, 200, $headers);
    }
}
