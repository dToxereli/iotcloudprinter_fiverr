<?php

namespace App\Http\Controllers;

use App\Device;
use App\DeviceGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CreateOrUpdateDeviceRequest;
use Response;

class DeviceController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $devices = Device::with(['device_groups', 'sales'])->get();
        return view('devices.index', compact('devices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $devicegroups = DeviceGroup::with('devices')->withCount('devices')->get();
        return view('devices.create', compact('devicegroups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateOrUpdateDeviceRequest $request)
    {
        if (Auth::user()->device_limit <= Auth::user()->devices->count()) {
            $request->session()->flash('flash_message', 'You have reached your device limit. Contact the administrator to assign you more');
            $request->session()->flash('flash_message_type', 'danger');
            return redirect()->back()->withInput();
        }
        $input = $request->validated();
        $device = Device::create($input);
        if (isset($input['device_group_ids']))
        {
            $device->device_groups()->sync($input['device_group_ids']);
        }
        Auth::user()->devices()->save($device);
        return redirect()->route('devices.show', $device);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Device $device)
    {
        return view('devices.show', compact("device"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Device $device)
    {
        $devicegroups = DeviceGroup::with('devices')->withCount('devices')->get();
        return view('devices.edit', compact('devicegroups', 'device'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateOrUpdateDeviceRequest $request, Device $device)
    {
        $input = $request->validated();
        $device->fill($input);
        if (isset($input['device_group_ids']))
        {
            $device->device_groups()->sync($input['device_group_ids']);
        } else {
            $device->device_groups()->sync([]);
        }
        $device->save();
        return redirect()->route('devices.show', $device);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Device $device)
    {
        $device->delete();
        return redirect()->route('devices.index');
    }

    public function export()
    {
        $headers = [
                'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=devices.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        $devices = Device::with(['sales', 'device_groups'])->get();

        $data = $devices->map(function ($device)
        {
            return [
                $device->name, 
                $device->created_at, 
                $device->is_active ? 'Active' : 'Disabled',
                $device->sales->count(),
                $device->sales->sum('total'),
                implode($device->device_groups->pluck('name')->toArray(), ","),
            ];
        })->toArray();

        array_unshift($data, ['Device', 'Date added', 'Status', 'Sales', 'Gross', 'Groups']);

        $callback = function() use ($data) 
        {
            $FH = fopen('php://output', 'w');
            foreach ($data as $row) { 
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return Response::stream($callback, 200, $headers);
    }
}