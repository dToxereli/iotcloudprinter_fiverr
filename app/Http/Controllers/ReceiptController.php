<?php

namespace App\Http\Controllers;

use App\Sale;
use Throwable;
use App\Product;
use App\SalePayment;
use App\SaleProduct;
use Illuminate\Http\Request;
use App\Utilitites\PDFParser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreReceiptRequest;

class ReceiptController extends Controller
{
      /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:devices');
    }

    public function store(StoreReceiptRequest $request)
    {
        $path = $request->file('receipt')->store('receipts');

        $fullpath = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().$path;
        $parser = new PDFParser();

        $content = [];

        try {
            $content = $parser->parse($fullpath);
        } catch (Throwable $th) {
            Storage::delete($path);
            return response()->json(['errors' => ['receipt' => 'Invalid receipt format']], 422);
        }
        $saledetails = $content['details'];
        $saledetails['receipt'] = $path;

        $existingsale = Sale::where('reference_number',$saledetails['reference_number'])->first();

        if (isset($existingsale)) {
            Storage::delete($path);
            return response()->json(['errors' => ['receipt' => 'The receipt already exists in the system']], 422);
        }

        $sale = Sale::create($saledetails);
        
        foreach ($content['products'] as $productdata) {
            $product = Product::firstOrCreate(['name' => $productdata['name']]);
            SaleProduct::create([
                'price' => $productdata['price'],
                'quantity' => $productdata['quantity'],
                'product_id' => $product->id,
                'sale_id' => $sale->id,
            ]);
        }

        SalePayment::create([
            'amount' => $saledetails['total'],
            'method' => $saledetails['method'],
            'sale_id' => $sale->id,
        ]);

        $device = Auth::user();
        $device->sales()->save($sale);
        return response()->json(['sale' => $sale], 201);
    }
}
