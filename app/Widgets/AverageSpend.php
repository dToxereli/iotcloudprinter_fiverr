<?php

namespace App\Widgets;

use App\Sale;
use Arrilot\Widgets\AbstractWidget;

class AverageSpend extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $average_spend = round(Sale::average('total'), 2);

        return view('widgets.average_spend', [
            'config' => $this->config,
            'average_spend' => $average_spend,
        ]);
    }
}
