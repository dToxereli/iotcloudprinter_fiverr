<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Lava;
use App\SalePayment;

class PaymentMethodGross extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $payments = SalePayment::all()->groupBy('method');

        $datatable = Lava::DataTable();
        $datatable->addStringColumn('Method')->addNumberColumn("Gross");

        $payments->each(function ($payments, $index) use ($datatable) {
            $datatable->addRow([$index, $payments->sum('amount')]);
        });

        $options = [
            'height' => 300
        ];

        $chart = Lava::PieChart('payment_method_gross', $datatable, $options);

        return view('widgets.payment_method_gross', [
            'config' => $this->config,
            'chart' => $chart,
        ]);
    }
}
