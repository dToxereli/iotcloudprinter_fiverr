<?php

namespace App\Widgets;

use App\Sale;
use Carbon\Carbon;
use Lava;
use Arrilot\Widgets\AbstractWidget;

class SalesGrossChart extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'summary' => 'sales',
        'span' => 'all_time',
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $span = $this->config['span'];
        $summary = $this->config['summary'];

        switch ($span) {
            case 'today':
                $sales = Sale::today()->get()->groupBy(function($sale) {
                    return Carbon::parse($sale->sale_date)->format('H');
                });
                break;
            case 'this_week':
                $sales = Sale::thisWeek()->get()->groupBy(function($sale) {
                    return Carbon::parse($sale->sale_date)->format('D');
                });
                break;
            case 'this_month':
                $sales = Sale::thisMonth()->get()->groupBy(function($sale) {
                    return Carbon::parse($sale->sale_date)->format('M d');
                });
                break;
            case 'this_year':
                $sales = Sale::thisYear()->get()->groupBy(function($sale) {
                    return Carbon::parse($sale->sale_date)->format('M');
                });
                break;
            default:
                $sales = Sale::all()->groupBy(function($sale) {
                    return Carbon::parse($sale->sale_date)->format('Y-M');
                });
                break;
        }

        $data = $sales->map(function ($salegroup, $datetime) use ($summary) {
            if ($summary == "sales") {
                return $salegroup->count();
            }
            return $salegroup->sum('total');
        });

        $datatable = Lava::DataTable();
        $datatable->addDateColumn('Period')->addNumberColumn($summary);

        $data->each(function ($value, $index) use ($datatable) {
            $datatable->addRow([$index, $value]);
        });

        $options = [
            'vAxis' => [ 
                'minorGridlines' => [ 'color' => 'transparent' ],
            ],
            'hAxis' => [ 
                'minorGridlines' => [ 'color' => 'transparent' ],
            ],
            'height' => 300
        ];

        $chart = Lava::AreaChart('sales_gross_chart', $datatable, $options);

        return view('widgets.sales_gross_chart', [
            'config' => $this->config,
            'chart' => $chart,
        ]);
    }
}
