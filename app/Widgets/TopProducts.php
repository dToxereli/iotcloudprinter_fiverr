<?php

namespace App\Widgets;

use App\Product;
use Arrilot\Widgets\AbstractWidget;

class TopProducts extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $top_products = Product::withCount('sales')->get()
                                ->sortByDesc('sales_count')
                                ->take(5);

        return view('widgets.top_products', [
            'config' => $this->config,
            'top_products' => $top_products,
        ]);
    }
}
