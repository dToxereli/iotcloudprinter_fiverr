<?php

namespace App\Widgets;

use App\Product;
use Arrilot\Widgets\AbstractWidget;

class TopSellingProducts extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'span' => 'all_time',
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $span = $this->config['span'];

        $products;

        switch ($span) {
            case 'today':
                $products = Product::with('sale_products')->soldToday()->get();
                break;
            case 'this_week':
                $products = Product::with('sale_products')->soldThisWeek()->get();
                break;
            case 'this_month':
                $products = Product::with('sale_products')->soldThisMonth()->get();
                break;
            case 'this_year':
                $products = Product::with('sale_products')->soldThisYear()->get();
                break;
            default:
                $products = Product::with('sale_products')->get();
                break;
        }

        $top_products = $products->sortByDesc('sold_quantity')->take(5);

        return view('widgets.top_selling_products', [
            'config' => $this->config,
            'top_products' => $top_products,
        ]);
    }
}
