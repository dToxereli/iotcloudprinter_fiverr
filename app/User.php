<?php

namespace App;

use App\Sale;
use App\Device;
use Carbon\Carbon;
use App\Traits\HasUuid;
use App\Traits\HasApiKey;
use QCod\ImageUp\HasImageUploads;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use HasImageUploads;
    use HasUuid;
    use HasApiKey;

    public $incrementing = false;

    public $api_key_field = 'api_token';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'device_limit', 'is_active', 'picture'
    ];

    protected static $imageFields = [
        'picture' => [
            'width' => 512,
            
            'height' => 512,

            'crop' => True,

            'disk' => 'public',

            'rules' => 'image|max:5000',

            'path' => 'profile_pictures',

            'placeholder' => '/images/prev.jpg',
        ]
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function devices()
    {
        return $this->hasMany(Device::class, 'user_id', 'id');
    }

    public function sales()
    {
        return $this->hasManyThrough(Sale::class, Device::class);
    }

    public function getLastLoginAttribute()
    {
        return Carbon::createFromTimeStamp(strtotime($this->last_login_at))->diffForHumans();
    }
}
