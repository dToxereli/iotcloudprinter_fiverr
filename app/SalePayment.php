<?php

namespace App;

use App\Sale;
use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;

class SalePayment extends Model
{
    use HasUuid;

    public $incrementing = false;
    
    protected $fillable = ['amount', 'method', 'sale_id'];

    public function sale()
    {
        return $this->belongsTo(Sale::class, 'sale_id', 'id');
    }
}
