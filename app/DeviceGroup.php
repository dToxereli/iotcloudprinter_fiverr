<?php

namespace App;

use App\Device;
use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;

class DeviceGroup extends Model
{
    use HasUuid;

    public $incrementing = false;
    
    protected $fillable = ['name'];
    
    public function devices()
    {
        return $this->belongsToMany(Device::class, 'devices_device_groups', 'device_group_id', 'device_id');
    }

    public function getTotalSalesAttribute()
    {
        return $this->devices->sum(function ($device) { return $device->sales->count(); });
    }

    public function getGrossSalesAttribute()
    {
        return $this->devices->sum(function ($device) { return $device->sales->sum('total'); });
    }
}
