
# IOT Scanner

This project is the web end of an IoT scanner project made specifically for @gupilkapoor from fiverr.

## Table of contents

- [IOT Scanner](#iot-scanner)
  - [Table of contents](#table-of-contents)
  - [Important notes](#important-notes)
    - [System requirements](#system-requirements)
  - [Installation](#installation)
  - [Testing](#testing)
  - [Usage](#usage)
    - [Register an account](#register-an-account)
    - [Adding devices](#adding-devices)
    - [Uploading pdf receipt files](#uploading-pdf-receipt-files)

## Important notes

This project was developed and tested on an Ubuntu 18.04 client and server. While it should be able to work on any operating system that supports PHP, there is no guarantee that all features will map effotlessly to other operating systems

### System requirements

The project was developed with laravel 5.8 and so all requirements of laravel 5.8 apply to this project

- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- BCMath PHP Extension

More details on the requirements of laravel can be found [on the laravel project documentation](https://laravel.com/docs/5.7)

In addition to these requirements, the project will be using a database later so we recommend you get a working installation of sqlite, mysql or postgres.

To install javascript requirements listed in package-lock.json, you will need to install node and npm

To be able to parse pdf files, the pdftotext utility is used. Install it with the commands below if you don't already have it installed in your system

**Debian/Ubuntu**: `sudo apt-get update && sudo apt-get install -y xpdf`

**Debian/Ubuntu**: `sudo yum install poppler-utils`

## Installation

Once you decompress the zip file, or clone this repository, cd into the project root directory using your preferred shell program (e.g. cmd on windows or terminal in linux or osx)

The files from this repository exclude libraries that were used during development. THerefore these libraries have to be installed separately.

Installing php dependencies

`composer install`

Installing nodejs dependencies

`npm install`

After running this two commands, setup the env files

`cp .env.example .env`

At this stage, the project does not include any database access so for the momment, it is not necessary to setup database configurations. However, we recommend that **you set up the right configuration now** since the project will be  including database access in the future. All database configurations can be made in the .env file

Once you have done your environment setup, run the following commands

```en
php artisan key:generate
php artisan storage:link
```

If you are using a linux os, you need to setup correct permissions so that your web server can write to these specific folders.

```en
sudo chgrp -R www-data bootstrap/cache/ storage/
sudo chmod -R g+rwx bootstrap/cache/ storage/
```

## Testing

To test the application, run the following command from the project root

`php artisan serve`

The internal php server should be on at 127.0.0.1:8000 or a subsequent port if that particular port is occupied

## Usage

### Register an account

Once the application loads, click on dashboard to login. If you don't have an account yet, go to register to create a new account. Fill in your details and sign up. Once logged in, you will be taken to the dashboard

### Adding devices

You can add devices to the platform

Click on devices and go to device management. A list of devices present on the sytem will be displayed along with a green button for add device.

Click on add device. A new form will load and you will be taken to it. Set the device name, status and groups (if any are available) and add the device to the system

Once added you will be taken to the device details page. Take note of the api token since you will need it to make any api requests to the platform. Each device you add will receive a unique api token

### Uploading pdf receipt files

Once you have noted the device token, you can make an api request with it. The url for uploading files is <http://yourdomain.com/api/receipt> which accepts only post requests. You can set the api token in either of 3 ways:

1. **Recommended** Setting the Authorization header to Bearer {device token} e.g. Bearer iQbGAnO6AzdTdZNhOa910f17CxNjC62zJETtGUpztRCJwZjVxowFCrPEOesc

2. Adding a form parameter with api_token as the key and your device token as the value

3. **Not recommended** Using the url <http://yourdomain.com/api/receipt/?api_token={device_token}>
   e,g, <http://yourdomain.com/api/receipt/?api_token=iQbGAnO6AzdTdZNhOa910f17CxNjC62zJETtGUpztRCJwZjVxowFCrPEOesc>
